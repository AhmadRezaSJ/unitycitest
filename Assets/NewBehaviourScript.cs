using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    private int[,] _input1 = new[,]
    {
        { 1, 1, 0, 1, 0 },
        { 1, 1, 0, 0, 0 },
        { 0, 0, 1, 0, 0 },
        { 0, 0, 0, 1, 1 }

    };
    
    
    private Dictionary<Vector2Int, List<Vector2Int>> _lands = new Dictionary<Vector2Int, List<Vector2Int>>();

    private HashSet<Vector2Int> _checkedCoords = new HashSet<Vector2Int>();

    private Queue<Vector2Int> _currentLandCoordsToCheck = new Queue<Vector2Int>();
    // Start is called before the first frame update
    void Start()
    {
        int[,] input = _input1;
        
        int rows = input.GetLength(0);
        int columns = input.GetLength(1);

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                Vector2Int coord = new Vector2Int(i, j);
                if (input[i, j] == 1 && !_checkedCoords.Contains(coord))
                {
                    _lands[coord] = new List<Vector2Int>();
                    _currentLandCoordsToCheck.Enqueue(coord);
                    DiscoverLand(input, coord, rows, columns);
                }
            }
        }


        foreach (KeyValuePair<Vector2Int,List<Vector2Int>> land in _lands)
        {
            Debug.Log($"{land.Key} - {land.Value.Count}");
            foreach (var VARIABLE in land.Value)    
            {
                Debug.Log(VARIABLE);
            }
        }
    }

    private void DiscoverLand(int[,] map, Vector2Int dictKey, int maxRows, int maxColumns)
    {
        while (_currentLandCoordsToCheck.Count > 0)
        {
            Vector2Int currentCoord = _currentLandCoordsToCheck.Dequeue();
            _lands[dictKey].Add(currentCoord);
            
            _checkedCoords.Add(currentCoord);

            
            Vector2Int up = new Vector2Int(currentCoord.x, currentCoord.y - 1);
            Vector2Int down = new Vector2Int(currentCoord.x, currentCoord.y + 1);
            Vector2Int left = new Vector2Int(currentCoord.x - 1, currentCoord.y);
            Vector2Int right = new Vector2Int(currentCoord.x + 1, currentCoord.y);

            Vector2Int[] adjacents = new[] { up, down, left, right };


            foreach (Vector2Int adjacent in adjacents)
            {
                if (IsInLand(map, adjacent, maxRows, maxColumns))
                {
                    _currentLandCoordsToCheck.Enqueue(adjacent);
                    
                    _checkedCoords.Add(adjacent);
                }
            }
        }
    }

    private bool IsInLand(int[,] map, Vector2Int coord, int maxRows, int maxColumns)
    {
        return (coord.x >= 0 && coord.x < maxRows) && (coord.y >= 0 && coord.y < maxColumns) && !_checkedCoords.Contains(coord) && map[coord.x, coord.y] == 1;
    }
}
