using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ProjectBuilder : MonoBehaviour
{
    public static void BuildAll()
    {
#if UNITY_EDITOR
        PlayerSettings.WebGL.memorySize = 1024;
        BuildPipeline.BuildPlayer(
            new[] { "Assets/Scenes/SampleScene.unity",
                  },
            "Builds/Windows/",
            BuildTarget.StandaloneWindows,
            BuildOptions.None);
#endif
    }    
}
