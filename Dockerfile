FROM unityci/editor:ubuntu-2021.3.6f1-webgl-1.0.1 AS builder
WORKDIR /igloovy/

RUN ls

ARG UNITY_USERNAME
ARG UNITY_PASSWORD

COPY Unity_lic.ulf /root/.local/share/unity3d/Unity/
COPY build.sh build.sh
COPY ./.env .
COPY Packages /igloovy/Packages/
COPY ProjectSettings /igloovy/ProjectSettings/
COPY Assets /igloovy/Assets/

RUN chmod +x build.sh
RUN ./build.sh ${UNITY_USERNAME} ${UNITY_PASSWORD}


FROM nginx:alpine
COPY  nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /igloovy/Builds /application/Builds

EXPOSE 80/tcp